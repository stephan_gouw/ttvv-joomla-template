<?php
/*
# ------------------------------------------------------------------------
# Extensions for Joomla 2.5.x - Joomla 3.x
# ------------------------------------------------------------------------
# Copyright (C) 2011-2015 Ext-Joom.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2.
# Author: Ext-Joom.com
# Websites:  http://www.ext-joom.com 
# Date modified: 02/02/2015 - 13:00
# ------------------------------------------------------------------------
*/
defined('_JEXEC') or die;

// Include the necessary file and create the resizer instance
require_once JPATH_SITE . '/plugins/content/imgresizecache/resize.php';
$resizer = new ImgResizeCache();


$item_heading 		= $params->get('item_heading', 'h4');
$ext_show_images	= $params->get('ext_show_images', 1);
$ext_image_url		= $params->get('ext_image_url', 1);
$ext_wordlimit		= (int)$params->get('ext_wordlimit', 10);
$ext_show_introtext = $params->get('ext_show_introtext', 1);

$json_decode_images = json_decode($item->images);
//print_r($json_decode_images);
$ext_img_article = strip_tags($item->introtext, '<img>');
$ext_img_article = preg_match('/<img[^>]+>/i', $ext_img_article, $ext_matches_img_article);
//print_r($ext_matches_img_article);
?>

<div class="ext-item-wrap">

	<?php if ($ext_show_images == 1) : ?>
		<div class="ext-item-img ext-item-img-article">
			<?php		
			if ( $ext_image_url > 0 ) {
				echo '<a href="'.$item->link.'">'.$ext_matches_img_article[0].'</a>';
					} else {
						echo $ext_matches_img_article[0]; 					
						}
			?>
		</div>
	<?php endif;  ?>

	<?php if ($json_decode_images->image_intro && $ext_show_images == 2) : ?>
		<div class="ext-item-img ext-item-img-intro">
			<?php		
			if ( $ext_image_url > 0 ) {
				echo '<a href="'.$item->link.'"><img src="'.JURI::base().$resizer->resize($json_decode_images->image_intro, array('w' => 700, 'h' => 358, 'crop' => TRUE)).'" alt="'.$json_decode_images->image_intro_alt.'" /></a>';
					} else {
						echo '<img src="'.JURI::base().$resizer->resize($json_decode_images->image_intro, array('w' => 700, 'h' => 358, 'crop' => TRUE)).'" alt="'.$json_decode_images->image_intro_alt.'" />';
						}
			?>
		</div>
	<?php endif;  ?>

	<?php if ($json_decode_images->image_intro && $ext_show_images == 3) : ?>
		<div class="ext-item-img ext-item-img-fulltext">
			<?php		
			if ( $ext_image_url > 0 ) {
				echo '<a href="'.$item->link.'"><img src="'.JURI::base().$resizer->resize($json_decode_images->image_fulltext, array('w' => 1240, 'h' => 634, 'crop' => TRUE)).'" alt="'.$json_decode_images->image_fulltext_alt.'" /></a>';
					} else {
						echo '<img src="'.JURI::base().$resizer->resize($json_decode_images->image_fulltext, array('w' => 1240, 'h' => 634, 'crop' => TRUE)).'" alt="'.$json_decode_images->image_fulltext_alt.'" />';
						}
			?>
		</div>
	<?php endif;  ?>




	<?php if ($params->get('item_title')) : ?>

		<<?php echo $item_heading; ?> class="newsflash-title<?php echo $params->get('moduleclass_sfx'); ?>">
		<?php if ($params->get('link_titles') && $item->link != '') : ?>
			<a href="<?php echo $item->link; ?>">
				<?php echo $item->title; ?>
			</a>
		<?php else : ?>
			<?php echo $item->title; ?>
		<?php endif; ?>
		</<?php echo $item_heading; ?>>

	<?php endif; ?>

	<?php if ($params->get('show_afterDisplayTitle')) : ?>
		<?php echo $item->afterDisplayTitle; ?>
	<?php endif; ?>
	
	<?php if ($params->get('show_beforeDisplayContent')) : ?>
		<?php echo $item->beforeDisplayContent; ?>
	<?php endif; ?>

	<?php 
	if ($ext_show_introtext > 0)
	{
		// Introtext
		$item->introtext = preg_replace('/<img[^>]*>/', '', $item->introtext);				
		// Word limit
		$item->introtext = ModExtArticlesNewsHelper::wordLimit($item->introtext, (int)$params->get('ext_wordlimit', 10));		
		//Clean the plugin tags
		$item->introtext = preg_replace("#{(.*?)}(.*?){/(.*?)}#s", '', $item->introtext);
		echo '<p>' . $item->introtext .'</p>';
	}
	?>

	<?php if (isset($item->link) && $item->readmore != 0 && $params->get('readmore')) : ?>
		<?php echo '<a class="readmore" href="' . $item->link . '">' . $item->linkText . '</a>'; ?>
	<?php endif; ?>

</div>