TTVV Joomla Template
=======================
Created by Stephan Gouw. Template forked from masterbootstrap.com. See the official site for more info: http://www.masterbootstrap.com

This template is based in Twitter Bootstrap 3, for Joomla! 3.x. 
***

### Some features:

+ 100% Bootstrap 3.5
+ HTML 5
+ Modernizr
+ Font Awesome
+ PIE for IE
+ Holder, client-side image placeholders
+ Upload Logo from Administrator
+ Fast and Light
+ Positions 100% managed for class CSS
+ Left and Right Modules with independent proportions
+ A Fullwidth position for your sliders
+ Hide front content option, from admin

### Attention: this version is stable, but under development

### Support

### SASS